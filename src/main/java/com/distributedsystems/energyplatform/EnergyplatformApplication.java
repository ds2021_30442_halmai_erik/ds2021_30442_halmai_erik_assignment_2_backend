package com.distributedsystems.energyplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyplatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnergyplatformApplication.class, args);
    }

}
